---
title: Third Person Tracking Camera
subtitle: A path following in game camera. 
image: /assets/img/portfolio/02-tracking-camera-full.png
alt: tracking-camera-image

caption:
  title: Third Person Tracking Camera
  subtitle: A path following in game camera. 
  thumbnail: /assets/img/portfolio/02-tracking-camera-thumbnail
  extension: .png
images:
  - image_path: \assets\img\thirdpersontrackingcamera\itDoesAngls
  - image_path: \assets\img\thirdpersontrackingcamera\cameraIsPerf
images2:
  - image_path: \assets\img\thirdpersontrackingcamera\SHcam
  - image_path: \assets\img\thirdpersontrackingcamera\SHcam2
  - image_path: \assets\img\thirdpersontrackingcamera\HGcam
  - image_path: \assets\img\thirdpersontrackingcamera\IPcam
button-texts:
  - button-text: Unity Asset page
    button-link: http://u3d.as/2ssN
  - button-text: Documentation
    button-link: https://nyangire.gitlab.io/nyangireworks/assets/docs/ThirdPersonTrackingCamerav1.0Documentation.pdf
---


{% include buttons.html buttons=page.button-texts %}

## About

Third Person Tracking camera is a camera system that follows objects in real-time constrained on a Bezier curve path. 

<br>

<div class='embed-container'>
<iframe width="560" height="315" src="https://www.youtube.com/embed/9_8xLNCfTqo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>

The camera can determine its position on the path in order for the object to best fit within the cameras frustum and user defined borders. It can have preset view angles on the Bezier path or follow the object freely. 

This asset was made with Unity to allow fixed angle and dolly type camera scenes during gameplay when the player has full control of the player avatar, the camera can be used exclusively or in conjunction with other third person cameras for special areas and scenes. The camera is fully automatic, requiring no input from the player.  



### Camera In the Editor

{% include photo-gallery.html images=page.images video="true" %}

This project was inspired by old school third person horror games where the camera would have certain fixed positions but also animate on specific paths in regards to the player character, allowing for a more cinematic framing and memorable sequences within the gameplay.



### Old School Examples

{% include photo-gallery.html images=page.images2 video="true"%}

The goal of this project is to make this camera asset be able to replicate and match such behavior, offer customization for various different behaviors, be very quick to setup for fast prototyping.

The camera can be used standalone to create a full third person old-school fixed/dolly camera type game or can be used in conjunction with other camera scrips for specific gameplay moments and tight areas.



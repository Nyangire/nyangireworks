---
caption: #what displays in the portfolio grid:
  title: Third Person Camera With Lock On
  subtitle: A action game style camera with an automatic lock on mode.
  thumbnail: /assets/img/portfolio/02-third-person-camera-thumbnail
  extension : .png
#what displays when the item is clicked:
title: Third Person Camera With Lock On
subtitle: A action game style camera with an automatic lock on mode.
image: /assets/img/portfolio/02-third-person-camera-full
extension: .png
alt: third-person-camera-image

images:
  - image_path: /assets/img/thirdpersoncamera/Capture
    title: Devil May Cry 4
    extension: .PNG
  - image_path: /assets/img/thirdpersoncamera/Capture2
    title: Drakengard 3
    extension: .PNG
  - image_path: /assets/img/thirdpersoncamera/Capture3
    title: Bayonetta
    extension: .PNG
  - image_path: /assets/img/thirdpersoncamera/Capture4
    title: Devil May Cry 5
    extension: .PNG
images2:
  - image_path: /assets/img/thirdpersoncamera/thrdperson
    title: Third Person
    extension: .jpg
  - image_path: /assets/img/thirdpersoncamera/ezgif.com-gif-maker
    extension: .jpg
    title: Lock On
button-texts: 
  - button-text: Unity Asset page
    button-link: https://assetstore.unity.com/packages/tools/camera/third-person-camera-with-lock-on-151550#description
  - button-text: Documentation
    button-link: https://nyangire.gitlab.io/nyangireworks/assets/docs/ThirdPersonCameraWithLockOnv1.0Documentation.pdf
---
{% include buttons.html buttons=page.button-texts %}

<!-- you can [get the PDF]({{site.baseurl}}/assets/docs/ThirdPersonCameraWithLockOnv1.0Documentation.pdf) directly.
<embed src="{{site.baseurl}}/assets/docs/ThirdPersonCameraWithLockOnv1.0Documentation.pdf" type="application/pdf"/>

<object data="{{site.baseurl}}/assets/docs/ThirdPersonCameraWithLockOnv1.0Documentation.pdf" width="1000" height="1000" type='application/pdf'/> -->
## About

The Third Person Camera with Lock On is a third person orbiting camera as seen in third person adventure games with the added functionality of a lock on feature designed for use with third person action games.

<br>

<div class='embed-container'>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Iowd4LI55EU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>

The Camera operates in two modes : Third person and Lock On



{% include photo-gallery.html images=page.images2 %}





The Third Person mode is a simple orbiting camera with collisions that follows the player character with customizable distances, behaviors and limits.





The Lock On mode is inspired by Japanese action games that keep the character in frame and roughly occupying the same screen space. This screen space is customizable and the camera automatically calculates distances and angles in order to keep the character on screen within margins. The camera is also moveable in lock on mode and constrains its movement based on the distances of the player and target such that the orbiting range is never too big.

### Action Game examples



{% include photo-gallery.html images=page.images %}





This projects goal was to create a lock on system for action games that was general enough to allow a varied level design rather then limit the type of environment suitable for the camera. 

It achieves that with its lock on algorithm that allows for very steep angles, very far or close enemies allowing proper locking on to gigantic enemies, far away targets and enemies hiding in more complex levels. This allows for the single camera to be used in very large arenas with colossal enemies much larger then the player character, close quarters combat taking place in corridors and rooms and vertical levels where enemies could be above or below.